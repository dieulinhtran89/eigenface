\documentclass[10pt]{article}
\usepackage{geometry}
\geometry{a4paper, top=25mm, left=30mm, right=25mm, bottom=30mm,
         headsep=10mm, footskip=12mm}
\usepackage[pdftex,colorlinks=true,citecolor=blue,urlcolor=blue,linkcolor=blue]{hyperref}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{caption}
\usepackage{subcaption}
\lstset{
    numberstyle=\footnotesize, 
    basicstyle=\footnotesize\ttfamily,
    numbers=left, 
    numbersep=5pt, 
    frame=single
}

\lstset{
    numberstyle=\footnotesize, 
    basicstyle=\footnotesize\ttfamily,
    numbers=none, 
    numbersep=5pt, 
    frame=single,
    breaklines=true,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
}


\title{Eigenface - Implementation and Evaluation\\Report}

\author{Linh Tran}

\date{\today}

\begin{document}
\maketitle
\tableofcontents
\newpage
\section{Introduction}
Humans are able to recognize and describe objects and scenes without thinking twice.
Among others, face recognition is an easy task for humans.
Experiments in \cite{turati2006newborns} have shown, that even babies are capable to differentiate known faces.
It is much more difficult for machines.
Face recognition is a challenging problem in computer vision and has received increased attention over the last years. 
It has various applications in systems for security, access control, gaming, image search and augmented reality.
Face recognion is a composite task, involving detection and location of faces, normalization, recognition and verication. 
These subtasks are aggravated by clutter and background variance, large variation in lighting direction, and changing facial expression. 
Further, depending on the application, noise, sizes of training and testing data, and speed requirements can be challenging.

Over the last decades, many methods have been proposed for face recognition. 
Subspace methods have been successfully applied to numerous recognition tasks such as tracking, object recognition, face localization and face recognition.
In particular, Principal Component Analysis (PCA) \cite{sirovich1987low, turk1991face, belhumeur1997eigenfaces, yang2002kernel, moghaddam1995probabilistic} and Fisher Linear Discriminant (FLD) \cite{belhumeur1997eigenfaces, yang2002kernel} methods have been employed for face recognition due to their impressive performance results.

Eigenface is the PCA-based algorithm for face recognition. 
The idea to use principal components to human face representation was first published by Sirovich and Kirby \cite{sirovich1987low} and used for face detection and recognition by Turk and Pentland \cite{turk1991face}.
The approach aims to capture the variation of face images and use this information to encode images. 
The comparison of images occur in a holistic manner, opposed to feature-based or parts-based.
Concretely, the eigenfaces are the eigenvectors (principle components) of the covariance matrix of the set of face images, where an image with N pixels is considered a vector in N-dimensional space.
The motivation of Eigenface is two-fold:

\begin{itemize}
\item Face images are represented by the relevant facial information, which is the statistical variation between face images. Thus, feature extraction do not need to depend on extracting specific face features such as the eyes, nose, and lips.
\item Reduce computation and space complexity, as images are efficiently represented by a small number of features.
\end{itemize}

However, some unwanted variations due to lighting, viewing points, facial expressions, etc. may be maintained when extracting a subspace with PCA. 
In \cite{adini1997face}, Adini et al. observed that variations between images of the same face due to lighting and viewing direction are almost always larger than variations of different face images. 
Still, the Eigenface algorithm is one of the first working facial recognition approach and have been applied to commercial face recognition products. 
Eigenface is still often considered as a baseline comparison method to demonstrate the minimum expected performance of such a system.

This report will present the implementation and evaluation of the Eigenface algorithm. I will describe the methematical background Eigenface algorithm in Section \ref{section:eigenface}. Section \ref{section:implementation} will highlight my Python implementation of Eigenface for face recognition. Finally, the evaluation results of Eigenface for Support Vector Machine (SVM) and Neural Networks (NN) are explained in Section \ref{section:evaluation}. Concluding, I will summarize the report and point out important challenges for future work in Section \ref{section:conclusion}.

\section{Eigenface}\label{section:eigenface}

In this section, I will summarize the algorithm of Eigenface in Subsection \ref{subsection:algo} and the relationship between Singular Value Decomposition (SVD) and PCA in \ref{subsection:svd:pca}. Both subsections are the foundation for my Eigenface implementation, which I will give overview to in the next Section \ref{section:implementation}.

\subsection{Algorithmic Description}\label{subsection:algo}
Let $X = \{x_1, ..., x_n\} \in \mathbb{R}^{n \times d}$ be a random vector with n observations $x_i \in \mathbb{R}^d$.
\begin{enumerate}
\item Compute the mean $\mu$ of $X$:
\begin{align}
\mu = \frac{1}{n} \sum_{i=1}^n x_i
\end{align}
\item Compute the covariance matrix S:
\begin{align}
S = \frac{1}{n} \sum_{i=1}^n (x_i-\mu)(x_i-\mu)^T
\end{align}
\item Compute the eigenvalues $\lambda_i$ and eigenvectors $v_i$ of S
\begin{align}
Sv_i = \lambda_i v_i, \; i = 1,2,...,n
\end{align}
\item Order the eigenvectors descending by their eigenvalues. The k principal components are the eigenvectors corresponding to the k largest eigenvalues.
\end{enumerate}

The k principal components of the observation $x$ are then computed as follows:
\begin{align}
y = (x-\mu)W^T
\end{align}
where $W = (v_1,...,v_k)$.

The reconstration from the PCA basis is as follows:
\begin{align}
x = Wy + \mu
\end{align}

The Eigenface algorithm can be used to perform face recognition in the following way:
\begin{itemize}
\item Projecting all training samples into the PCA subspace.
\item Projecting the test image into the PCA subspace.
\item Finding the nearest neighbor between the projected training images and the projected test
image.
\end{itemize}

For finding the nearest neighbor between projected training images and projected test image, Support Vector Machine and a simple feed-forward Neural Network (NN) will be used to verify the performance of Eigenface. 
See Section \ref{section:implementation} and \ref{section:evaluation} for implementation details and results.

Determining the eigenvectors of C is an intractable task for typical image sizes if $d >> n$. For efficiently compute the eigenvectors, the eigenvectors can be computed from a much more smaller $n \times n$ matrix, as a $m \times n$ matrix with $m > n$ can only have $N-1$ non-zero eigenvalues.Therefore, the eigenvalue decomposition $S=X^T X$ of size $n \times n$ can be used instead:

\begin{align}
X^T X v_i = \lambda_i v_i 
\end{align}

The original eigenvectors of $S = X X^T$ can be computed as follows:
\begin{align}
XX^T (X v_i) = \lambda_i (X v_i)
\end{align}

The resulting eigenvectors are orthogonal. To get orthonormal eigenvectors they need to be normalized to unit length. Please look into \cite{duda2012pattern} for derivation and proof.
 
\subsection{PCA and SVD}\label{subsection:svd:pca}
While formally both PCA and SVD can be used to calculate the same principal components and their corresponding eigenvalues, the extra step of calculating the covariance matrix  can lead to numerical rounding errors when calculating the eigenvalues/vectors. As I am using SVD in my implementation, this subsection discusses the similarity between PCA and SVD. 

PCA requires the computation of eigenvalues and eigenvectors of the covariance matrix, which is the product $XX^T$, where X is the data matrix. Since the covariance matrix is symmetric, the matrix is diagonalizable, and the eigenvectors can be normalized such that they are orthonormal:

\begin{align}
XX^T=WDW^T \label{eq:pca}
\end{align}

Using SVD to decompose X gives us:

\begin{align}
X = U \Sigma V^T 
\end{align}

Assume the covariance matrix is given by $S = \frac{1}{n} X X^T$, the covariance matrix can be formulated as:

\begin{align}
S = \frac{1}{n} (U \Sigma V^T)^T(U \Sigma V^T) = (V \Sigma^T U^T)^T(U \Sigma V^T)
\end{align}

Since U is orthogonal, $U^T U = I$:

\begin{align}
S = V \Sigma^2 V^T \label{eq:svd}
\end{align}

and the correspondence between Equations \ref{eq:pca} and \ref{eq:svd} is easily seen.

\section{Implementation}\label{section:implementation}
This section gives a brief overview over the necessary implementation for the Eigenface algorithm.
Translating the PCA from the algorithmic description of section \ref{subsection:algo} to Python is almost trivial, as we can use the SVD implementation of scikit-learn \cite{scikit-learn}. 
The source code is available in folder lib/pca.py. 
Listing \ref{lst:pca:py} shows parts of class PCA, which implements the Principal Component Analysis given by steps 1-4 in Subsection \ref{subsection:algo}. 
SVD was employed to calculate the principal axis instead of computing the covariance matrix and calculating the eigenvectors. Further, PCA whitening was used have an identity covariance matrix. This allowed better classification results.

\lstinputlisting[caption={\href{../lib/pca}{lib/pca.py} \label{lst:pca:py}}, language=python, linerange={6-54}]{../lib/pca.py}

For classification, Eigenface  was used to feed the input to SVM and a simple Neural Network. For SVM, the implementation by scikit-learn \cite{scikit-learn} was used. For the NN, a 3 layer (input, hidden, output) network was implemented with TensorFlow \cite{tensorflow2015-whitepaper} which supports regularization, class weights and dropout. See lib/neural\_networks.py for implementation details. 

\subsection{Availability}
All code is publicly available and can be cloned from the following bitbucket repository: \\

\href{https://dieulinhtran89@bitbucket.org/dieulinhtran89/eigenface.git}{https://dieulinhtran89@bitbucket.org/dieulinhtran89/eigenface.git}
\\
\noindent Table \ref{table:overview:code} gives an overview over available the modules in the repository.
For more information about the ORL and LFW database, please refer toSubsection \ref{subsection:data}.

\begin{table}[H]
\centering
\caption{Overview of modules in repository.}
\label{table:overview:code}
\begin{tabular}{|l||l|}
\hline
Source & Description \\
\hline
\hline
eigenface\_lfw.ipynb & iPython notebook which executes evaluation for LFW dataset. \\
\hline
eigenface\_lfw.py & Python script version of eigenface\_lfw.ipynb \\
\hline
eigenface\_orl.ipynb & iPython notebook which executes evaluation for ORL dataset.\\ \hline
eigenface\_orl.py & Python script version of eigenface\_orl.ipynb\\ \hline
data/ & Directory to store data sets, plots and intermediate resuls. \\ \hline
data/att\_faces/ & Directory containing the AT\&T ORL dataset. \\ \hline
lib/classification\_report.py & Parsing classification report. \\ \hline
lib/dataset.py & Implementation of dataset objects for handling data \\ \hline
lib/neural\_networks.py & Implementation of a simple feedforward Neural Network. \\ \hline
lib/pca.py & Implementation of PCA for Eigenface\\ \hline
lib/plot.py & Several functions for plotting\\ \hline
lib/util.py & Functions to process image vectors.\\ \hline
report/eigenface\_lfw.pdf & iPython notebook results of evaluation with LFW (min. face = 70) \\
\hline
report/eigenface\_orl.pdf & iPython notebook results of evaluation with ORL \\ \hline
report/main.pdf & Report of Eigenface implementation and evaluation \\ \hline
\end{tabular}
\end{table}

\section{Evaluation}\label{section:evaluation}

The evaluation consist of a short description of the datasets used (Subsection \ref{subsection:data}), the evaluation of Eigenface and SVM (Subsection \ref{subsection:svm:eig}), the evaluation of Eigenface and Neural Networks (Subsection \ref{subsection:nn:eig}).

\subsection{Face Database}\label{subsection:data}
In order to evaluate the Eigenface algorithm, two datasets were used, the AT\&T Facedatabase and the Labeled Faces in the Wild (LFW) database. 
\begin{description}
\item[\href{http://www.cl.cam.ac.uk/research/dtg/attarchive/facedatabase.html}{AT\&T Facedatabase}] The AT\&T Facedatabase, also known as ORL Database of Faces, contains for each 40 distinct subsjects ten different images. All imagages weren taken against a dark homogeneous background. The subject are all in an upright, frontal position. For many subjects, the images vary in lighting, and the faces vary in their facial expressions (eyes closed or opened, smiling or not smiling) and facial attributes (e.g. glasses).
\item[\href{http://vis-www.cs.umass.edu/lfw/}{Labeled Faces in the Wild}] The LFW dataset contains more than 13,000 images of faces collected from the web. They have been labeled with the name of the person. 1680 of these people have two or more distinct photos in the data set.

\end{description}
\subsection{Evaluation of Eigenface and SVM}\label{subsection:svm:eig}
For evaluating the performance of Eigenface and Support Vector Machine, we used the datasets describe in the previous subsection. Due to time and computational constraints, we extracted three datasets from LFW, each with different minimum number of images per person: 10, 30 and 70. Table \ref{table:overview:lfw:orl} gives overview over the applied datasets. 
\begin{table}[ht]
\centering
\caption{Overview of LFW and ORL dataset with varying number of images per person. The LFW images were already resized by 0.4.}
\label{table:overview:lfw:orl}
\begin{tabular}{|l||c|c|c|c|}
\hline
Dataset & Minimum number of images per person & \#samples & \#feature & \#classes \\ \hline \hline
LFW & 10 & 4,324 & 1,850 & 158 \\ \hline
LFW & 30 & 2,370 & 1,850 & 34 \\ \hline
LFW & 70 & 1,288 & 1,850 & 7 \\ \hline
ORL & 10 & 400 & 10,304 & 40 \\ \hline
\hline
\end{tabular}
\end{table}

Each dataset in Table \ref{table:overview:lfw:orl} was split in a training and test. 
The biggest dataset (LFW with 4,324 images) had a train-test split ratio of 3:1, while the other three had a train-test split ratio of 9:1. 
The decision to split 9:1 was due to the small data set size. 
Further, the training was used for 10-fold cross validation to estimate the best hyperparameter for SVM. 
The best hyperparameter was search in a grid search manner with hyperparameter C in range of $\{0.1, 1,..., 1000, 3000\}$ and $\gamma$ in range of $\{0.0001, 0.0005, ..., 1.0, 5.0\}$. 
The hyperparameter combination with the best average F1-score over all folds was then used to train the test set. 
Train and test set results can be found in Table \ref{table:overview:performance:svm}.

Eigenface produces excellent results for ORL and LFW with at least 70 faces per person. The confusion matrix and precision-recall curves of LFW (see Figures \ref{fig:cf:lfw:70} and \ref{fig:pr:lfw:70}) and ORL (see Figure \ref{fig:cf:orl}) illustrates the overall well classification. However, the F1 score and accuracy for LFW halved when reducing the minimum number of faces to 10. There are many possible reasons:
\begin{itemize}
\item The dataset is heavily imbalanced. The results are already improved by adding class weights to the SVM classification, but may still suffer under the imbalance.
\item As the name (Labeled Faces in the Wild) suggests, the LFW dataset contains all possible images, who vary in background, lighting, facial expressions and attributes, as well as viewing points. Normalization and histogram equalization was already applied to improve the results.
\end{itemize}

\begin{table}[ht]
\centering
\caption{Overview of SVM classification results of LFW and ORL dataset with varying number of images per person.}
\label{table:overview:performance:svm}
\begin{tabular}{|c|c||c|c|c|c|c|}
\hline
Dataset & Minimum number & Best & Average train& Test F1 & Test & Test \\ 
& of images & parameter & F1 score & score & accuracy & error rate\\
& per person &&&&&\\ \hline \hline
LFW & 10 & $C=10$& 0.437 & 0.41 & 44.03\% & 55.97\% (605/1081) \\ 
& & $\gamma=0.001$ &&&& \\ \hline
LFW & 30 & $C=1$ & 0.704 & 0.74 & 75.11\% & 24.89\% (59/237)\\
& & $\gamma=0.01$ &&&&\\ \hline
LFW & 70 & $C=100$ & 0.83 & 0.89 & 89.13\% & 10.87\% (35/322) \\
& & $\gamma=0.01$ &&&&\\ \hline
ORL & 10 & $C=100$ & 0.902 & 0.97 & 97.50\% & 2.50\% (1/40) \\
& & $\gamma=0.0005$ &&&&\\ \hline
\hline
\end{tabular}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{figures/confusion_matrix_70_lfw.png}
\caption{Confusion matrix of classification of LFW with at least 70 faces per person with Eigenface+SVM}\label{fig:cf:lfw:70}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/precision_recall_70_lfw.png}
\caption{Precision recall of classification of LFW with at least 70 faces per person with Eigenface+SVM} \label{fig:pr:lfw:70}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/confusion_matrix_orl.png}
\caption{Confusion matrix of classification of ORL with Eigenface+SVM}\label{fig:cf:orl}
\end{figure}

\subsection{Evaluation of Eigenface and Neural Networks}\label{subsection:nn:eig}
In order to compare the performance of SVM, a simple feedforward Neural Network was implemented. The initial parameters of the NN are given below:
\begin{description}
\item[Type] Feed forward back propagation network.
\item[Number of layers] 3 (input, one hidden, output layer)
\item[Number of neurons in input layer] Number of eigenfaces.
\item[Number of neurons in hidden layer] 75 for ORL, 40 for LFW with 70 faces per person 
\item[Number of neurons in output layer] Number of classes.
\item[Transfer function of the hidden layer] Relu
\item[Training computation] Logits + cross-entropy loss
\item[Number of instances per batch] 20
\end{description}

The training set was used to estimate the best learning rate $\alpha$. The model was then trained with the optimize learning rate and tested it on the test set. Results can be found in Table \ref{table:overview:performance:nn} and plots of performance across the epochs can be found in Figures \ref{fig:pr:nn:lfw} and \ref{fig:pr:nn:orl}.

\begin{table}[ht]
\centering
\caption{Overview of Neural Network classification results of LFW and ORL dataset}
\label{table:overview:performance:nn}
\begin{tabular}{|c|c||c|c|c|c|}
\hline
Dataset & Minimum number & Best & Train & Test & Test \\ 
& of images & parameter & accuracy & accuracy & F1 score\\
\hline \hline
LFW & 70 & $\alpha=0.3$ & 68.84\% & 71.12\% & 0.67 \\ \hline
ORL & 10 & $\alpha=1.0$ & 76.67\% & 75.00\% & 0.70 \\ \hline
\hline
\end{tabular}
\end{table}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/nn_lfw_hidden_40_input_75_rate_0_3.png}
\caption{Error vs. epoch for the Neural Network training and test performances of LFW} \label{fig:pr:nn:lfw}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{figures/nn_orl_hidden_120_input_150_rate_1.png}
\caption{Error vs. epoch for the Neural Network training and test performances of ORL} \label{fig:pr:nn:orl}
\end{figure}

For a relatively simple NN model, the networks achieved over 75 \% accuracy for ORL and LFW with 70 faces per person. 
However, compared to the SVM results, the classification with NN performs significantly worse. 
I can imagine that a more sophisticated model, e.g. n neural networks for n classes, each network classifying for one class and making a majority decision, would improve the classification performance.


\section{Conclusion}\label{section:conclusion}

This report has shown that Eigenface is an effective algorithm to recognize faces, but fails to classify faces if there is a high amount in variance in background, noise, and facial expression and attributes. Also, there is the challenge of imbalanced datasets which also downgrades the performance of the classification.

Since Eigenface, many other face recognition methods have been published \cite{schroff2015facenet, liu2015targeting, ding2015robust, masi2016we} which accomplish excellent results for face recognition. Particularly, the development of deep learning and the availability of big datasets make significant improvement in face recognition possible. 

\newpage

\bibliography{references} 
\bibliographystyle{ieeetr}

\end{document}
