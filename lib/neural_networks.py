from lib.classification_report import ClassificationReport
from lib.util import prepare_labels
from numpy import mean, argmax
import sys
import tensorflow as tf


class NeuralNetwork:
    '''
    Implements a simple feed-forward neural network network
    with one hidden layer.
    Weighted classes are used to balance the dataset (optional).
    L2 regularizers (optional) and dropout are employed
    to avoid overfitting.
    '''
    def __init__(self, n_input, n_hidden, n_output,
                 regularizers=True, batch=1,
                 weighted=True, weights=None):
        # Number of input, hidden and output neurons
        self.n_input = n_input
        self.n_hidden = n_hidden
        self.n_output = n_output
        # Weighted bool and values
        self.weighted = weighted
        self.weights = weights
        # drop out variable
        self.keep_prob = tf.placeholder("float")
        self.model(
            n_input, n_hidden, n_output,
            regularizers=regularizers
        )
        self.learning_rate = [
            1.0, 0.3, 0.1, 0.04, 0.03, 0.02, 0.01,
            0.003, 0.001, 0.0003, 0.0001
        ]
        # batch size for training
        self.batch = batch

    # initialize weights with a specific shape
    def init_weights(self, shape):
        return tf.Variable(tf.random_normal(shape, stddev=0.01))

    # set up
    def forwardprop(self):
        # Fully connected layer. Note that the '+' operation automatically
        # broadcasts the biases.
        h = tf.nn.relu(tf.matmul(self.X, self.w_1) + self.fc1_biases)
        # Add a 50% dropout during training only. Dropout also scales
        # activations such that no rescaling is needed at evaluation time.
        h_drop = tf.nn.dropout(h, self.keep_prob)
        return tf.matmul(h_drop, self.w_2) + self.fc2_biases

    # Use gradient descent for the optimization
    def get_gd_optimizer(self, lr):
        return tf.train.GradientDescentOptimizer(lr).minimize(self.loss)

    def model(self, n_input, n_hidden, n_output, regularizers=True):
        '''
        Model a neural network with given parameters.
        :param n_input: Number of input nodes:
        :param n_hidden: Number of hidden nodes
        :param n_output: Number of outcomes
        :param regularizers: Whether to use L2 regularization or not
        '''

        # Symbols
        self.X = tf.placeholder("float", shape=[None, n_input])
        self.y = tf.placeholder("float", shape=[None, n_output])

        # Weight initializations
        self.w_1 = self.init_weights((n_input, n_hidden))
        self.w_2 = self.init_weights((n_hidden, n_output))
        self.fc1_biases = tf.Variable(tf.constant(0.1, shape=[n_hidden]))
        self.fc2_biases = tf.Variable(tf.constant(0.1, shape=[n_output]))

        # Forward propagation
        yhat = self.forwardprop()

        # Add class weights
        if self.weighted:
            class_weight = tf.constant(self.weights, dtype=tf.float32)
            yhat = tf.mul(yhat, class_weight)

        self.predict = tf.argmax(yhat, dimension=1)
        correct_prediction = tf.equal(tf.argmax(yhat, 1), tf.argmax(self.y, 1))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

        # Backward propagation
        self.loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(yhat, self.y))

        # Adding regularizers
        if regularizers:
            l2 = (
                tf.nn.l2_loss(self.w_1) + tf.nn.l2_loss(self.fc1_biases) +
                tf.nn.l2_loss(self.w_2) + tf.nn.l2_loss(self.fc2_biases)
            )
            self.loss += 5e-4 * l2

    # Small utility function to evaluate a dataset by feeding batches
    # of data to train the model and evaluate the given test set
    def evaluate_learning_rate(self, X_train, X_test, y_train,
                               y_test, n_classes, learning_rate=None,
                               stdout=True, label_names=None):

        y_train_matrix = prepare_labels(y_train, n_classes)
        y_test_matrix = prepare_labels(y_test, n_classes)

        # used either learning rate given or predefined ones
        if learning_rate:
            params = learning_rate
        else:
            params = self.learning_rate

        # store epoch results for evaluation
        results = {
            'epoch': [],
            'train_accuracy': [],
            'test_accuracy': [],
            'test_f1': [],
            'test_error': []
        }

        # Evaluate with each learning rate
        for lr in params:
            with tf.Session() as sess:
                # initialize all variables defined during modeling
                init = tf.initialize_all_variables()
                sess.run(init)

                # feed batches of training data at each epoch
                # saves memory and enables this to run on smaller GPUs
                for i in range(len(X_train)/self.batch + 1):
                    start = i*self.batch
                    end = min((i + 1)*self.batch, len(X_train) - 1)
                    d = {
                        self.X: X_train[start:end],
                        self.y: y_train_matrix[start:end],
                        self.keep_prob: 0.5
                    }

                    sess.run(self.get_gd_optimizer(lr), feed_dict=d)
                    y_train_pred = sess.run(
                        self.predict,
                        feed_dict={
                            self.X: X_train, self.y: y_train_matrix,
                            self.keep_prob: 1.0
                        }
                    )
                    train_accuracy = mean(
                        argmax(y_train_matrix, axis=1) == y_train_pred)
                    y_test_pred = sess.run(
                        self.predict,
                        feed_dict={
                            self.X: X_test, self.y: y_test_matrix,
                            self.keep_prob: 1.0
                        }
                    )
                    test_accuracy = mean(
                        argmax(y_test_matrix, axis=1) == y_test_pred)
                    report = ClassificationReport(
                        y_test, y_test_pred, target_names=label_names)
                    if stdout:
                        print("Learning rate = %s, epoch = %d, train accuracy"
                              " = %.2f%%, test accuracy = %.2f%%, test f1"
                              " = %.2f%%" % (
                                    lr, i + 1, 100. * train_accuracy,
                                    100. * test_accuracy,
                                    100*report.get_avg_f1_score()
                              )
                        )
                        sys.stdout.flush()
                    results['epoch'].append(i)
                    results['train_accuracy'].append(train_accuracy)
                    results['test_accuracy'].append(test_accuracy)
                    results['test_f1'].append(100*report.get_avg_f1_score())
                    results['test_error'].append(
                        (argmax(y_test_matrix, axis=1) == y_test_pred).sum())
                print("Learning rate = %s, train accuracy"
                      " = %.2f%%, test accuracy = %.2f%%, test f1 = %.2f%%" % (
                            lr, 100. * train_accuracy,
                            100. * test_accuracy,
                            100*report.get_avg_f1_score())
                         )
                sys.stdout.flush()
        return results
