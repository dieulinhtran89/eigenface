from math import sqrt
from numpy import mean, dot, newaxis, argsort
from numpy.linalg import svd


class PCA:
    '''
    Implements Principal Component Analysis
    '''
    def __init__(self, X, n_components=0, whiten=True):
        n_samples, n_features = X.shape
        if n_components <= 0 or n_components > n_features:
            self._n_components = n_features
        else:
            self._n_components = n_components
        self._mu = mean(X, axis=0)
        self._whiten = whiten
        self.compute(X, n_samples)

    def compute(self, X, n_samples):
        # Using SVD to decomposite X and get the principal components
        # More accurate than computing covariance and then calculate
        # eigenvalues.
        X = X - self._mu
        U, S, V = svd(X, full_matrices=False)

        # Sort eigenvectors descending by their eigenvalue
        idx = argsort(-S)
        S = S[idx]
        V = V[idx, :]

        # Get variance explained by singular values
        explained_variance_ = (S ** 2) / n_samples
        total_var = explained_variance_.sum()
        self._explained_variance_ratio = \
            explained_variance_[:self._n_components] / total_var

        # select only num_components
        S = S[:self._n_components]
        V = V[:self._n_components, :]

        # whitening PCA
        # transforming data to have an identity covariance matrix
        if self._whiten:
            self._components = V / S[:, newaxis] * sqrt(n_samples)
        else:
            self._components = V
        self.eigenvalues = S

    # transform data with components
    def transform(self, X):
        if self._mu is None:
            return dot(X, self._components.T)
        return dot(X - self._mu, self._components.T)

    # return components
    def get_components(self, n=0):
        if 0 < n < self._n_components:
            return self._components[:n]
        return self._components

    # get explained variance for each components
    def get_explained_variance_ratio(self):
        return self._explained_variance_ratio

    # returns mean of X
    def get_mu(self):
        return self._mu