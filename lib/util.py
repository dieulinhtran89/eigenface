from numpy import (
    asarray, min, max, eye
)


def scale(X, min_val, max_val):

    X = asarray(X)
    min_X = min(X)
    max_X = max(X)
    return min_val + (max_val - min_val) * (X - min_X) / (max_X - min_X)


def prepare_labels(y, n_classes):
    return eye(n_classes)[y]
