from lib.util import prepare_labels
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from numpy import newaxis, arange, asarray
from sklearn.metrics import (
    confusion_matrix, precision_recall_curve,
    average_precision_score
)


def plot_confusion_matrix(cm, labels, title='Confusion matrix',
                          cmap=cm.Blues, figsize=(7,7), filename=None):
    '''
    Plots confusion matrix.
    :param cm: confusion matrix
    :param labels: List of labels to index the matrix.
    :param title: title for
    :param cmap: color map
    :param figsize: size of figure
    :param filename: if defined, the plot will be saved to that filename
    '''

    fig, ax = plt.subplots(figsize=figsize)
    ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.set_title(title, fontsize=14)
    tick_marks = arange(len(labels))
    ax.set_xticks(tick_marks)
    ax.set_xticklabels(labels, rotation=90)
    ax.set_yticks(tick_marks)
    ax.set_yticklabels(labels)
    ax.tick_params(axis='both', labelsize=14)
    plt.tight_layout()
    ax.set_ylabel('True label', fontsize=14)
    ax.set_xlabel('Predicted label', fontsize=14)
    if filename:
        fig.savefig(filename)


def plot_normalized_confusion_matrix(y, y_pred, n_classes, labels,
                                     figsize=(7,7), filename=None):
    '''
    Plots normalized confusion matrix
    :param y: Ground truth (correct) target values.
    :param y_pred: Estimated targets as returned by a classifier.
    :param n_classes: Number of classes.
    :param labels: List of labels to index the matrix.
    :param figsize: Size of Figure.
    :param filename: If defined, the plot will be save to that filename.
    '''
    cm = confusion_matrix(y, y_pred, labels=range(n_classes))
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, newaxis]
    plot_confusion_matrix(
        cm_normalized, labels, title='Normalized Confusion Matrix',
        figsize=figsize, filename=filename
    )

# create dict with font and fontsize
def create_font(fontname='Tahoma', fontsize=10):
    return {'fontname': fontname, 'fontsize':fontsize}


def plot_eigenface(title, images, rows, cols, sptitle="subplot", sptitles=[],
                   colormap=cm.gray, filename=None):
    '''
    Plot the best 18 eigenfaces.
    :param title: Title of plot.
    :param images: Eigenfaces
    :param rows: Number of rows in plot.
    :param cols: Number of columns in plot.
    :param sptitle: Title for all subplot.
    :param sptitles: Individual titles for subplot.
    :param colormap: Color map.
    :param filename: If defined, the plot will be save to that filename.
    '''
    fig = plt.figure(figsize=(15,10))
    fig.text(.5, .95, title, horizontalalignment='center', fontsize=18)
    for i in xrange(len(images)):
        ax0 = fig.add_subplot(rows,cols,(i+1))
        plt.setp(ax0.get_xticklabels(), visible=False)
        plt.setp(ax0.get_yticklabels(), visible=False)
        if len(sptitles) == len(images):
            plt.title("%s #%s" % (sptitle, str(sptitles[i])), create_font('Tahoma',15))
        else:
            plt.title("%s #%d" % (sptitle, (i+1)), create_font('Tahoma',15))
        plt.imshow(asarray(images[i]), cmap=colormap)
    if filename:
        fig.savefig(filename)


def plot_precision_recall_curves(y_test, y_score, n_classes,
                                 label_names=None, figsize=(10,7),
                                 filename=None):
    '''
    Plot precision recall curves for each class.
    :param y_test: Ground truth (correct) target values.
    :param y_score: Distances of the samples to the separating hyperplane.
    :param n_classes: Number of classes
    :param label_names: List of labels to index the matrix.
    :param figsize: Size of Figure.
    :param filename: If defined, the plot will be save to that filename.
    '''
    precision = dict()
    recall = dict()
    average_precision = dict()
    y_test_matrix = prepare_labels(y_test, n_classes)

    for i in range(n_classes):
        precision[i], recall[i], _ = precision_recall_curve(
            y_test_matrix[:, i], y_score[:, i])
        average_precision[i] = average_precision_score(
            y_test_matrix[:, i], y_score[:, i])
        # Compute micro-average ROC curve and ROC area
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_test_matrix.ravel(), y_score.ravel())
        average_precision["micro"] = average_precision_score(
            y_test_matrix, y_score, average="micro")

    # Plot Precision-Recall curve for each class
    fig = plt.figure(figsize=figsize)
    label = 'micro-average Precision-recall curve (area = {0:0.2f})'.format(
        average_precision["micro"])
    plt.plot(recall["micro"], precision["micro"], label=label)
    for i in range(n_classes):
        label = ('Precision-recall curve of class {0} '
                 '(area = {1:0.2f})'.format(label_names[i], average_precision[i]))
        plt.plot(recall[i], precision[i], label=label)

    # settings for axis
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall', fontsize=14)
    plt.ylabel('Precision', fontsize=14)
    plt.tick_params(labelsize=14)
    plt.title(
        'Extension of Precision-Recall curve to multi-class',
        fontsize=14
    )
    plt.legend(
        loc='upper center', bbox_to_anchor=(0.5, -0.1),
        shadow=True, fontsize=14
    )
    if filename:
        fig.savefig(filename)


def plot_epochs(epoch, train_accuracy, test_accuracy, figsize=(10,7)):
    '''
    Plot accuracy of train and test set at each epoch.
    :param epoch: Epoch list.
    :param train_accuracy: List of train accuracy at each epoch.
    :param test_accuracy: List of test accuracy at each epoch.
    :param figsize: Size of Figure.
    '''
    plt.figure(figsize=figsize)
    plt.plot(epoch, train_accuracy, label='Train')
    plt.plot(epoch, test_accuracy, label='Test')
    plt.ylim([0.0, 1.05])
    plt.xlabel('Epoch', fontsize=14)
    plt.ylabel('Accuracy', fontsize=14)
    plt.tick_params(labelsize=14)
    plt.legend(
        loc='upper center', bbox_to_anchor=(0.5, -0.1),
        shadow=True, fontsize=14
    )
