import cPickle as pickle
from numpy import (
    asarray, uint8, unique, bincount,
    min, floor,
    histogram, arange, cumsum, sum
)
import os
import PIL.Image as Image
from sklearn.cross_validation import train_test_split
from sklearn.datasets import fetch_lfw_people

# path to ORL images
att_face_path = os.path.join(os.getcwd(), 'data', 'att_faces')


def get_lfw_dataset(min_face=70, resize=0.4, preprocess=True):
    return LFWDataset(min_face=min_face, resize=resize, preprocess=preprocess)


def get_orl_dataset(preprocess=False):
    return ORLDataset(att_face_path, preprocess=preprocess)


class Dataset(object):
    '''
    Dataset for storing data sets, summarizing,
    and creating data splits
    '''
    def __init__(self, name, n_samples, n_features,
                 n_classes, min_face):
        self.name = name
        self.n_samples = n_samples
        self.n_features = n_features
        self.n_classes = n_classes
        self.min_face = min_face

    def summarize(self):
        print("Size of %s dataset:" % self.name)
        print(" - Number of samples: %s" % self.n_samples)
        print(" - Number of features: %s" % self.n_features)
        print(" - Number of classes: %s" % self.n_classes)
        print(" - Minimum number of faces per person: %s" % self.min_face)

    # creates train test split
    # optionally can be stored to file
    def create_train_test_split(self, test_size=0.1,
                                to_file=True, file_path=None):
        self.X_train, self.X_test, self.y_train, self.y_test = \
            train_test_split(self.X, self.y, test_size=test_size)

        def dump(f):
            pickle.dump(
                [self.X_train, self.X_test, self.y_train, self.y_test], f
            )

        if to_file:
            if file_path:
                with open(file_path, 'w+') as file:
                    dump(file)
            else:
                with open(self.split_file_path, 'w+') as file:
                    dump(file)
        return self.X_train, self.X_test, self.y_train, self.y_test

    # load existing train/test split
    # create new train/test split if file does not exists
    def load_train_test_split(self, file_path=None):
        if file_path and os.path.isfile(file_path):
            file = open(file_path)
        elif os.path.isfile(self.split_file_path):
            file = open(self.split_file_path)
        else:
            return self.create_train_test_split()
        return pickle.load(file)

    # intensity normalization
    def normalize(self, i):
        lmin = float(i.min())
        lmax = float(i.max())
        return floor((i - lmin) / (lmax - lmin) * 255.0)

    # histogram equalization
    def equalize(self, i):
        h = histogram(i, bins=arange(256))[0]
        H = cumsum(i) / float(sum(h))
        e = floor(H[i.flatten().astype('int')] * 255.0)
        return e.reshape(i.shape)


class ORLDataset(Dataset):
    '''
    Loads ORL dataset from file.
    '''
    def __init__(self, path, preprocess=False):
        self.images, self.y = self.read_images(path)
        n_samples, self.h, self.w = self.images.shape
        self.X = self.images.reshape([n_samples, self.h*self.w])
        if preprocess:
            for i in range(n_samples):
                self.X[i, :] = self.equalize(self.normalize(self.X[i, :]))
        n_features = self.X.shape[1]
        super(ORLDataset, self).__init__(
            "ORL", n_samples, n_features,
            len(unique(self.y)), min(bincount(self.y)))
        self.split_file_path = os.path.join(
            os.getcwd(), 'tmp', 'orl_split_data.p')

    # read image from file
    def read_images(self, path):
        c = 0
        X = []
        y = []
        for root, dirs, files in os.walk(path):
            for directory in dirs:
                subject_path = os.path.join(root, directory)
                for filename in os.listdir(subject_path):
                    try:
                        im = Image.open(os.path.join(subject_path, filename))
                        im = im.convert("L")
                        X.append(asarray(im, dtype=uint8))
                        y.append(c)
                    except Exception as e:
                        print "Unexpected error:", e
                        raise
                c += 1
        return asarray(X), asarray(y)


class LFWDataset(Dataset):
    '''
    Loads LFW dataset from sklearn library.
    min_face: minimum number of images per person
    resize: image resize factor
    '''
    def __init__(self, min_face=70, resize=0.4, preprocess=True):
        lfw_dataset = fetch_lfw_people(
            min_faces_per_person=min_face, resize=resize)
        # get images as numpy arrays
        X = lfw_dataset.data

        # get labels of X and the corresponding label names
        self.y = lfw_dataset.target
        self.label_names = lfw_dataset.target_names

        # get number of samples, height, width of images,
        # number of features and number of classes
        self.X = lfw_dataset.data
        n_samples, self.h, self.w = lfw_dataset.images.shape

        # preprocess
        if preprocess:
            for i in range(n_samples):
                self.X[i, :] = self.equalize(self.normalize(self.X[i, :]))

        super(LFWDataset, self).__init__(
            "LFW", n_samples, X.shape[1], self.label_names.shape[0], min_face)
        self.split_file_path = os.path.join(
            os.getcwd(), 'tmp',
            'split_data_{min_face}.p'.format(
                min_face=self.min_face)
        )
