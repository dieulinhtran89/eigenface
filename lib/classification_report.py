from sklearn.metrics import (
    classification_report
)


class ClassificationReport():
    '''
    Processing classification report to extract specific
    scores.
    '''
    def __init__(self, y, y_pred, target_names=None):
        cr = classification_report(
            y, y_pred, target_names=target_names)
        self.report = cr

        # processing lines of classification report
        for c, line in enumerate(cr.split("\n")):
            self.results = {}

            if c == 0 or not line:
                continue
            elif "avg / total" in line:
                line = line.split('  ')
                line = map(str.strip, line)
                line = filter(None, line)
                self.avg_precision = float(line[1])
                self.avg_recall = float(line[2])
                self.avg_f1_score = float(line[3])
                self.total_support = float(line[4])
            else:
                line = line.split('  ')
                line = map(str.strip, line)
                line = filter(None, line)
                self.results[line[0]] = {}
                self.results[line[0]]['precision'] = float(line[1])
                self.results[line[0]]['recall'] = float(line[2])
                self.results[line[0]]['f1-score'] = float(line[3])
                self.results[line[0]]['support'] = float(line[4])

    # get average f1 score
    def get_avg_f1_score(self):
        return self.avg_f1_score

    def __str__(self):
        print self.report

    def print_report(self):
        return self.__str__()
